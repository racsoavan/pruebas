class DashboardController < ApplicationController
	
  def info
  	@env = ENV
  	respond_to do |format|
	  format.html
	  format.json { render json: @env }
	end
  end
end
